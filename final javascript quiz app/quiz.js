
var timeleft = 3;
var downloadTimer = setInterval(function () {
    if (timeleft <= 0) {
        clearInterval(downloadTimer);
        document.getElementById("countdown").innerHTML = "";
        
        /* start of actual quiz program. */
        var myQuestions = [

            {
                question: "The ozone layer restrics?",
                answers: {
                    a: 'Visible light',
                    b: 'Infrared radiation',
                    c: 'X-rays and gamma rays',
                    d: 'Ultraviolet radiation'
                },
                correctAnswer: 'd'
            },
            {
                question: "Which of the following is used in pencils?",
                answers: {
                    a: 'Graphite',
                    b: 'Silicon',
                    c: 'Charcoal',
                    d: 'Phosphorous'
                },
                correctAnswer: 'a'
            },


            {
                question: "What is 30/3?",
                answers: {
                    a: '3',
                    b: '5',
                    c: '10'
                },
                correctAnswer: 'c'
            },
            {
                question: "How many days do we have in a week?",
                answers: {
                    a: 'Seven',
                    b: 'six',
                    c: 'five',
                    d: 'four'
                },
                correctAnswer: 'a'
            },
            {
                question: "Which animal is known as the 'Ship of the Desert?'",
                answers: {
                    a: 'Elephent',
                    b: 'Camel',
                    c: 'Dog',
                    d: 'Cow'
                },
                correctAnswer: 'b'
            }
        ];
        var quizContainer = document.getElementById('quiz');
        var resultsContainer = document.getElementById('results');
        var submitButton = document.getElementById('submit');
        document.getElementById('submit').style.display = 'table';
        function showQuestions(questions, quizContainer) {
            // we'll nees a place to store the output and the
            // answer choices
            
            var output = [];
            output.push ('<br /> <br />');
            var answers;

            // for each question.
            for (var i = 0; i < questions.length; i++) {
                // first reset the list of answers.
                answers = [];

                //for each availble answer to this question.
                for (letter in questions[i].answers) {
                    // add an html radio button.
                    answers.push(
                        '<label>'
                        + '<input type="radio" name="question'+i+'" value="'+letter+'">'
                        + letter + ': '
                        + questions[i].answers[letter]
                    +'</label>'
                    );
                }
                // add this question and its answers to the output.
                output.push(
                    '<div class="question">' + questions[i].question + '</div>'
                    + '<div class="answers">' + answers.join('') +  '<hr />'+ '</div>'
                );
            }
            // finally combine our output list  into one string of html
            // and put it on the page.
            quizContainer.innerHTML = output.join('');
        }
        function showResults(questions, quizContainer, resultsContainer) {
            // gather answer containers from our quiz.
            var answerContainer = quizContainer.querySelectorAll('.answers');

            //keep track of user's answers.
            var userAnswer = '';
            var numCorrect = 0;

            // for each question.
            for (var i = 0; i < questions.length; i++) {
                //find selected answer
                userAnswer = (answerContainer[i].querySelector('input[name=question' + i + ']:checked') || {}).value;

                //if answer is correct.
                if (userAnswer === questions[i].correctAnswer) {
                    // add to the number of correct answers.
                    numCorrect++;

                    //color the answer green.
                    answerContainer[i].style.color = 'green';
                    //answerContainer.innerHTML = "Correct!"
                }
                // if answer is wrong or blank.
                else {
                    answerContainer[i].style.color = 'red';
                   // answerContainer.innerHTML = "Incorrect!"
                }
            }
            //show number of correct answers out of total.
            resultsContainer.innerHTML =  '<p style="font-size:40px;">' + 'Your score is: ' + '<strong>'+numCorrect + ' out of '
                + questions.length + '<strong />'+'<p/>';
            
        }

        function generateQuiz(questions, quizContainer, resultsContainer, submitButton) {
            //shows the questions.
            showQuestions(questions, quizContainer);

            // when user clicks submit, show results.
            submitButton.onclick = function () {
                showResults(questions, quizContainer, resultsContainer);
            }

        }


        generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);
    }
    /* Else part of countdown timer. */
    else {
       
        document.getElementById("countdown").innerHTML = "quiz will start in: " + timeleft ;

    }
    timeleft -= 1;
}, 1000);
